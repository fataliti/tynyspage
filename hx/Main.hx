

import js.Browser.window;
import js.Browser.document;

class Main {
    static function main() {
        window.onload = function () {
            document.getElementById("footer").innerHTML = '<div style="width: 350px;">
<img src="res/imgs/kelogo.png" alt="главная" >
</div>
<div>
<p class = "footerUpP">
<a href="mailto:info@tynys.kz" class = "footerUpP">info@tynys.kz </a>
</p>
<p class = "footerDownP">
© Акционерное общество "Тыныс" 2021
</p> 
</div>
<div class = "footerRight">
<div class = "footerRightDiv">
<a href="https://www.youtube.com/channel/UC3I6sCkqRbIjTHm65aVSYAw"><img src="res/imgs/icons/youtube.png" class="footerSocial" > </a>  
<a href="https://www.facebook.com/profile.php?id=100065671702286"><img src="res/imgs/icons/face.png" class="footerSocial"> </a>  
<a href="https://instagram.com/aotynys"><img src="res/imgs/icons/instagram.png" class="footerSocial" > </a>  
<a href="https://go.2gis.com/869e2" target="_blank"><img src="res/imgs/location.png" alt="" class="footerLocation"></a>
<a href="tel:87162253789"><img src="res/imgs/phone_green.svg" class="footerPhoneIco"> </a>  
<p class="footerPhoneText">+7 7162 25 47 19</p> 
</div>
</div>';

            var header = document.getElementById("header");
            if (header != null) {
                header.innerHTML = "<ul>
<li><a href='index.html'>ГЛАВНАЯ</a> </li>
<li><a href='production.html'>ПРОДУКЦИЯ</a></li>
<li><a href='contacs.html'>КОНТАКТЫ</a></li>
<li><a href='about.html'>О ПРЕДПРИЯТИИ</a></li>
<li><a href='anticor.html'>АНТИКОР</a></li>
</ul>";
            }

        }
    }
}
